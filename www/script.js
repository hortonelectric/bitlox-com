(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var mr = new MatrixRain({
	tileWidth: 15,
	tileHeight: 15,
	fontSize: 15,
	numCols: 146,
	numRows: 103,
	placement: 'center',
	newThreads: 10,
	fillStyle: 'rgba( 0, 25, 0, .1 )',
	mfillStyle: 'rgba( 0, 25, 0, .9 )'
})

// more init stuff could go here

module.exports = mr

},{}],2:[function(require,module,exports){
var win = $( window )
var bod = $( 'body' )
var mr = require( './mr' )
var header = $('<header id="mainHeader">')
var logo = $('<img id="logo" src="logo2.svg">')
var center = $('<div>')
var btnPricing = $('<button id="btnPricing">View Pricing</button>' )
var downArrow = $('<a href="#mainContent" class="downArrow">')
var isResizing = true
var TOPBAR_HEIGHT = 60

$( init )

function init() {

	header.append( logo, btnPricing )

	center.css({
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate( -50%, -50% )'
	})
	bod.append( center, downArrow, header )
	center.append( mr.container )

	mr.container.css({
		position: 'relative',
		overflow: 'hidden',
		backgroundColor: 'black'
	})

	mr.mask = {
		img: $('<img src="logo.png">'),
		sample: {
			x: 113,
			y: 0,
			width: 146,
			height: 73,
		},
		offset: {
			x: 18,
			y: 15
		}
	}
	mr.start()
	win.on( 'resize', resize )
	resize()
	win.scroll( onScroll )
	setTimeout( logoIn, 2500 )

	function logoIn() {
		mr.newThreads = 2
		if( logo.stopAnim ) return
		logo.animate( { opacity: .8 }, {
			duration: 1500,
			easing: 'linear',
			complete: onLogoIn
		})
	}

	function onLogoIn() {
		var css
		isResizing = false
		if( logo.stopAnim ) return
		downArrow.addClass( 'shown' )
		btnPricing.addClass( 'shown' )
		css = {
			width: 250 * 1.5,
			height: 73 * 1.5,
			marginLeft: 0,
			marginTop: 0
		}
		logo.animate( css, stopAnim )
	}

	function stopAnim() {

		logo.stopAnim = true
		logo.clearQueue().finish()
		mr._mask.enabled = false
		mr._mcanvas.fadeOut()
		logo.css({ opacity: 1, width: 250, height: 73, marginLeft: 0, marginTop: 0 })
	}

	function onScroll() {
		var scrpos = win.scrollTop()
		var mch = mr.container.height()
		var tgt = mch/2 + win.height() / 2 - TOPBAR_HEIGHT
		var perc = scrpos / tgt, invPerc

		if( perc > 1 ) perc = 1; invPerc = 1 - perc

		if( scrpos > 0 && !logo.stopAnim ) stopAnim()
		if( scrpos === 0 && logo.stopAnim ) {
			downArrow.addClass( 'shown' )
		} else {
			downArrow.removeClass( 'shown' )
			if( logo.stopAnim ) btnPricing.addClass( 'shown' )
		}
		console.log( 'perc', perc )
		var ty = -50 * invPerc
		css = { top: (50*(1-perc)) +'%', transform: 'translateY(' + ty + '%)' }
		header.css( css )
		btnPricing.css({opacity: invPerc})
		if( scrpos > tgt ) {
			center.css({ position: 'fixed', top: -mch/2 + TOPBAR_HEIGHT } )
		} else {
			center.css({ position: 'absolute', top: '50%' })
		}
	}

	function resize() {

		var w = win.width()
		var h = win.height()
		var dw = w / mr.width
		var dh = h / mr.height
		var f = dw > dh ? dw : dh
		var css = { width: Math.floor( mr.width * f), height: Math.floor( mr.height * f ) }
		if( css.width % 2 ) css.width += 1
		if( css.height % 2 ) css.height += 1

		$('#mainContent').css( {marginTop: h/2 + css.height/2  })

		mr.container.css( css )
		onScroll()
		if( !isResizing ) return

		f = mr.height * f / 44
		css = { width: 250 * f * .45, height: 73 * f, marginLeft: -f * 19.5, marginTop: -f * .4 }
		if( !logo.stopAnim ) logo.css( css )

	}

}

},{"./mr":1}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy5udm0vdmVyc2lvbnMvbm9kZS92Ny4xMC4wL2xpYi9ub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwic2NyaXB0L21yLmpzIiwic2NyaXB0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgbXIgPSBuZXcgTWF0cml4UmFpbih7XG5cdHRpbGVXaWR0aDogMTUsXG5cdHRpbGVIZWlnaHQ6IDE1LFxuXHRmb250U2l6ZTogMTUsXG5cdG51bUNvbHM6IDE0Nixcblx0bnVtUm93czogMTAzLFxuXHRwbGFjZW1lbnQ6ICdjZW50ZXInLFxuXHRuZXdUaHJlYWRzOiAxMCxcblx0ZmlsbFN0eWxlOiAncmdiYSggMCwgMjUsIDAsIC4xICknLFxuXHRtZmlsbFN0eWxlOiAncmdiYSggMCwgMjUsIDAsIC45ICknXG59KVxuXG4vLyBtb3JlIGluaXQgc3R1ZmYgY291bGQgZ28gaGVyZVxuXG5tb2R1bGUuZXhwb3J0cyA9IG1yXG4iLCJ2YXIgd2luID0gJCggd2luZG93IClcbnZhciBib2QgPSAkKCAnYm9keScgKVxudmFyIG1yID0gcmVxdWlyZSggJy4vbXInIClcbnZhciBoZWFkZXIgPSAkKCc8aGVhZGVyIGlkPVwibWFpbkhlYWRlclwiPicpXG52YXIgbG9nbyA9ICQoJzxpbWcgaWQ9XCJsb2dvXCIgc3JjPVwibG9nbzIuc3ZnXCI+JylcbnZhciBjZW50ZXIgPSAkKCc8ZGl2PicpXG52YXIgYnRuUHJpY2luZyA9ICQoJzxidXR0b24gaWQ9XCJidG5QcmljaW5nXCI+VmlldyBQcmljaW5nPC9idXR0b24+JyApXG52YXIgZG93bkFycm93ID0gJCgnPGEgaHJlZj1cIiNtYWluQ29udGVudFwiIGNsYXNzPVwiZG93bkFycm93XCI+JylcbnZhciBpc1Jlc2l6aW5nID0gdHJ1ZVxudmFyIFRPUEJBUl9IRUlHSFQgPSA2MFxuXG4kKCBpbml0IClcblxuZnVuY3Rpb24gaW5pdCgpIHtcblxuXHRoZWFkZXIuYXBwZW5kKCBsb2dvLCBidG5QcmljaW5nIClcblxuXHRjZW50ZXIuY3NzKHtcblx0XHRwb3NpdGlvbjogJ2Fic29sdXRlJyxcblx0XHR0b3A6ICc1MCUnLFxuXHRcdGxlZnQ6ICc1MCUnLFxuXHRcdHRyYW5zZm9ybTogJ3RyYW5zbGF0ZSggLTUwJSwgLTUwJSApJ1xuXHR9KVxuXHRib2QuYXBwZW5kKCBjZW50ZXIsIGRvd25BcnJvdywgaGVhZGVyIClcblx0Y2VudGVyLmFwcGVuZCggbXIuY29udGFpbmVyIClcblxuXHRtci5jb250YWluZXIuY3NzKHtcblx0XHRwb3NpdGlvbjogJ3JlbGF0aXZlJyxcblx0XHRvdmVyZmxvdzogJ2hpZGRlbicsXG5cdFx0YmFja2dyb3VuZENvbG9yOiAnYmxhY2snXG5cdH0pXG5cblx0bXIubWFzayA9IHtcblx0XHRpbWc6ICQoJzxpbWcgc3JjPVwibG9nby5wbmdcIj4nKSxcblx0XHRzYW1wbGU6IHtcblx0XHRcdHg6IDExMyxcblx0XHRcdHk6IDAsXG5cdFx0XHR3aWR0aDogMTQ2LFxuXHRcdFx0aGVpZ2h0OiA3Myxcblx0XHR9LFxuXHRcdG9mZnNldDoge1xuXHRcdFx0eDogMTgsXG5cdFx0XHR5OiAxNVxuXHRcdH1cblx0fVxuXHRtci5zdGFydCgpXG5cdHdpbi5vbiggJ3Jlc2l6ZScsIHJlc2l6ZSApXG5cdHJlc2l6ZSgpXG5cdHdpbi5zY3JvbGwoIG9uU2Nyb2xsIClcblx0c2V0VGltZW91dCggbG9nb0luLCAyNTAwIClcblxuXHRmdW5jdGlvbiBsb2dvSW4oKSB7XG5cdFx0bXIubmV3VGhyZWFkcyA9IDJcblx0XHRpZiggbG9nby5zdG9wQW5pbSApIHJldHVyblxuXHRcdGxvZ28uYW5pbWF0ZSggeyBvcGFjaXR5OiAuOCB9LCB7XG5cdFx0XHRkdXJhdGlvbjogMTUwMCxcblx0XHRcdGVhc2luZzogJ2xpbmVhcicsXG5cdFx0XHRjb21wbGV0ZTogb25Mb2dvSW5cblx0XHR9KVxuXHR9XG5cblx0ZnVuY3Rpb24gb25Mb2dvSW4oKSB7XG5cdFx0dmFyIGNzc1xuXHRcdGlzUmVzaXppbmcgPSBmYWxzZVxuXHRcdGlmKCBsb2dvLnN0b3BBbmltICkgcmV0dXJuXG5cdFx0ZG93bkFycm93LmFkZENsYXNzKCAnc2hvd24nIClcblx0XHRidG5QcmljaW5nLmFkZENsYXNzKCAnc2hvd24nIClcblx0XHRjc3MgPSB7XG5cdFx0XHR3aWR0aDogMjUwICogMS41LFxuXHRcdFx0aGVpZ2h0OiA3MyAqIDEuNSxcblx0XHRcdG1hcmdpbkxlZnQ6IDAsXG5cdFx0XHRtYXJnaW5Ub3A6IDBcblx0XHR9XG5cdFx0bG9nby5hbmltYXRlKCBjc3MsIHN0b3BBbmltIClcblx0fVxuXG5cdGZ1bmN0aW9uIHN0b3BBbmltKCkge1xuXG5cdFx0bG9nby5zdG9wQW5pbSA9IHRydWVcblx0XHRsb2dvLmNsZWFyUXVldWUoKS5maW5pc2goKVxuXHRcdG1yLl9tYXNrLmVuYWJsZWQgPSBmYWxzZVxuXHRcdG1yLl9tY2FudmFzLmZhZGVPdXQoKVxuXHRcdGxvZ28uY3NzKHsgb3BhY2l0eTogMSwgd2lkdGg6IDI1MCwgaGVpZ2h0OiA3MywgbWFyZ2luTGVmdDogMCwgbWFyZ2luVG9wOiAwIH0pXG5cdH1cblxuXHRmdW5jdGlvbiBvblNjcm9sbCgpIHtcblx0XHR2YXIgc2NycG9zID0gd2luLnNjcm9sbFRvcCgpXG5cdFx0dmFyIG1jaCA9IG1yLmNvbnRhaW5lci5oZWlnaHQoKVxuXHRcdHZhciB0Z3QgPSBtY2gvMiArIHdpbi5oZWlnaHQoKSAvIDIgLSBUT1BCQVJfSEVJR0hUXG5cdFx0dmFyIHBlcmMgPSBzY3Jwb3MgLyB0Z3QsIGludlBlcmNcblxuXHRcdGlmKCBwZXJjID4gMSApIHBlcmMgPSAxOyBpbnZQZXJjID0gMSAtIHBlcmNcblxuXHRcdGlmKCBzY3Jwb3MgPiAwICYmICFsb2dvLnN0b3BBbmltICkgc3RvcEFuaW0oKVxuXHRcdGlmKCBzY3Jwb3MgPT09IDAgJiYgbG9nby5zdG9wQW5pbSApIHtcblx0XHRcdGRvd25BcnJvdy5hZGRDbGFzcyggJ3Nob3duJyApXG5cdFx0fSBlbHNlIHtcblx0XHRcdGRvd25BcnJvdy5yZW1vdmVDbGFzcyggJ3Nob3duJyApXG5cdFx0XHRpZiggbG9nby5zdG9wQW5pbSApIGJ0blByaWNpbmcuYWRkQ2xhc3MoICdzaG93bicgKVxuXHRcdH1cblx0XHRjb25zb2xlLmxvZyggJ3BlcmMnLCBwZXJjIClcblx0XHR2YXIgdHkgPSAtNTAgKiBpbnZQZXJjXG5cdFx0Y3NzID0geyB0b3A6ICg1MCooMS1wZXJjKSkgKyclJywgdHJhbnNmb3JtOiAndHJhbnNsYXRlWSgnICsgdHkgKyAnJSknIH1cblx0XHRoZWFkZXIuY3NzKCBjc3MgKVxuXHRcdGJ0blByaWNpbmcuY3NzKHtvcGFjaXR5OiBpbnZQZXJjfSlcblx0XHRpZiggc2NycG9zID4gdGd0ICkge1xuXHRcdFx0Y2VudGVyLmNzcyh7IHBvc2l0aW9uOiAnZml4ZWQnLCB0b3A6IC1tY2gvMiArIFRPUEJBUl9IRUlHSFQgfSApXG5cdFx0fSBlbHNlIHtcblx0XHRcdGNlbnRlci5jc3MoeyBwb3NpdGlvbjogJ2Fic29sdXRlJywgdG9wOiAnNTAlJyB9KVxuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIHJlc2l6ZSgpIHtcblxuXHRcdHZhciB3ID0gd2luLndpZHRoKClcblx0XHR2YXIgaCA9IHdpbi5oZWlnaHQoKVxuXHRcdHZhciBkdyA9IHcgLyBtci53aWR0aFxuXHRcdHZhciBkaCA9IGggLyBtci5oZWlnaHRcblx0XHR2YXIgZiA9IGR3ID4gZGggPyBkdyA6IGRoXG5cdFx0dmFyIGNzcyA9IHsgd2lkdGg6IE1hdGguZmxvb3IoIG1yLndpZHRoICogZiksIGhlaWdodDogTWF0aC5mbG9vciggbXIuaGVpZ2h0ICogZiApIH1cblx0XHRpZiggY3NzLndpZHRoICUgMiApIGNzcy53aWR0aCArPSAxXG5cdFx0aWYoIGNzcy5oZWlnaHQgJSAyICkgY3NzLmhlaWdodCArPSAxXG5cblx0XHQkKCcjbWFpbkNvbnRlbnQnKS5jc3MoIHttYXJnaW5Ub3A6IGgvMiArIGNzcy5oZWlnaHQvMiAgfSlcblxuXHRcdG1yLmNvbnRhaW5lci5jc3MoIGNzcyApXG5cdFx0b25TY3JvbGwoKVxuXHRcdGlmKCAhaXNSZXNpemluZyApIHJldHVyblxuXG5cdFx0ZiA9IG1yLmhlaWdodCAqIGYgLyA0NFxuXHRcdGNzcyA9IHsgd2lkdGg6IDI1MCAqIGYgKiAuNDUsIGhlaWdodDogNzMgKiBmLCBtYXJnaW5MZWZ0OiAtZiAqIDE5LjUsIG1hcmdpblRvcDogLWYgKiAuNCB9XG5cdFx0aWYoICFsb2dvLnN0b3BBbmltICkgbG9nby5jc3MoIGNzcyApXG5cblx0fVxuXG59XG4iXX0=
