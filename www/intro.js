(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

module.exports = navigator.userAgent.match(/Android/i)
|| navigator.userAgent.match(/webOS/i)
|| navigator.userAgent.match(/iPhone/i)
|| navigator.userAgent.match(/iPad/i)
|| navigator.userAgent.match(/iPod/i)
|| navigator.userAgent.match(/BlackBerry/i)
|| navigator.userAgent.match(/Windows Phone/i)
//|| true
? true : false

},{}],2:[function(require,module,exports){
var isMobile = require( './isMobile' )

var mr = new MatrixRain({
	numCols: isMobile ? 45 : 91,
	numRows: isMobile ? 31 : 61,
	placement: 'center'
})

mr.container.css({
	position: 'relative',
	overflow: 'hidden',
	backgroundColor: 'black'
})

module.exports = mr

},{"./isMobile":1}],3:[function(require,module,exports){
var isMobile = require( './isMobile' )
var floor = Math.floor
var win = $( window )
var bod = $( 'body' )
var mr = require( './mr' )
var center = $('#mr-center')
var lcon = $('#logo-con')
var logo = $('#logo-con img')
var topWrap = $('#top-wrap')
var top = $('#top')
var MID_X = isMobile ? 18 : 42
var MID_Y = isMobile ? 16 : 31
var cap1, cap2, cap3
var mask = mr.createMask()

center.append( mr.container )

mask.setImage( 'img/o-lock.png',
	isMobile ? 13 : 27,
	isMobile ? 10 : 19,
	isMobile ? 19 : 39,
	isMobile ? 13 : 25
)

$( init )

function init() {
	mr.start()
	win.on( 'resize', resize )
	//win.on( 'scroll', onScroll )
	resize()
	$('#mainMenu').css({display: 'block'})
	setTimeout( showCaption1, 1000 )

}

function showBanners() {

	var banners = [ 'THE FUTURE', 'IS NOW', 'BITLOX' ]
	nextBanner()

	function nextBanner() {
		var banner = banners.shift()
		banners.push( banner )
		mr.startBanner( banner )
		setTimeout( stopBanner, 10000 )
		function stopBanner() {
			mr.stopBanner()
			setTimeout( nextBanner, 3000 )
		}
	}

}

function showCaption1() {
	cap1 = mr.createCaption()
	cap1.start( 'T H E   F U T U R E', MID_X - 5, MID_Y )
	cap1.onLocked = function() {
		setTimeout( showCaption2, 500 )
	}
}

function showCaption2() {
	cap2 = mr.createCaption()
	cap2.start( 'I S   N O W', MID_X, MID_Y )
	cap2.onLocked = function() {
		setTimeout( launchMask, 500 )
	}
}

function launchMask() {
	mask.onFinished = function() {
		console.log( 'mask finished' )
		logo.addClass('enter')
		setTimeout(function(){logo.addClass('finish'); showBanners()}, 2000 )
		mask.stop()
	}
	mask.launch()
}

function onScroll() {
	var st = win.scrollTop()
	var wh = win.height()
	var ch = center.height()
	var tgt = wh - 100
	var perc = st / tgt
	var hh = ch * 0.5
	if( perc >= 1 ) perc = 1
	var bot = ch / 2 - perc * hh

	topWrap.css({height: wh - tgt * perc })
	if( perc === 1 ) {
		bod.addClass( 'fullin' )
	} else {
		bod.removeClass( 'fullin' )
	}

}

function resize() {

	var w = win.width()
	var h = win.height()
	var dw = w / mr.width
	var dh = h / mr.height
	var f = dw > dh ? dw : dh
	var css = {
		width: floor( mr.width * f),
		height: floor( mr.height * f )
	}

	if( css.width % 2 ) css.width += 1
	if( css.height % 2 ) css.height += 1

	$('#mainContent').css( {marginTop: h })

	mr.container.css(css)
	onScroll()

}

},{"./isMobile":1,"./mr":2}]},{},[3])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy5udm0vdmVyc2lvbnMvbm9kZS92Ny4xMC4wL2xpYi9ub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwic3JjL2ludHJvL2lzTW9iaWxlLmpzIiwic3JjL2ludHJvL21yLmpzIiwic3JjL2ludHJvIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJcbm1vZHVsZS5leHBvcnRzID0gbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvQW5kcm9pZC9pKVxufHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvd2ViT1MvaSlcbnx8IG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goL2lQaG9uZS9pKVxufHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvaVBhZC9pKVxufHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvaVBvZC9pKVxufHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvQmxhY2tCZXJyeS9pKVxufHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvV2luZG93cyBQaG9uZS9pKVxuLy98fCB0cnVlXG4/IHRydWUgOiBmYWxzZVxuIiwidmFyIGlzTW9iaWxlID0gcmVxdWlyZSggJy4vaXNNb2JpbGUnIClcblxudmFyIG1yID0gbmV3IE1hdHJpeFJhaW4oe1xuXHRudW1Db2xzOiBpc01vYmlsZSA/IDQ1IDogOTEsXG5cdG51bVJvd3M6IGlzTW9iaWxlID8gMzEgOiA2MSxcblx0cGxhY2VtZW50OiAnY2VudGVyJ1xufSlcblxubXIuY29udGFpbmVyLmNzcyh7XG5cdHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuXHRvdmVyZmxvdzogJ2hpZGRlbicsXG5cdGJhY2tncm91bmRDb2xvcjogJ2JsYWNrJ1xufSlcblxubW9kdWxlLmV4cG9ydHMgPSBtclxuIiwidmFyIGlzTW9iaWxlID0gcmVxdWlyZSggJy4vaXNNb2JpbGUnIClcbnZhciBmbG9vciA9IE1hdGguZmxvb3JcbnZhciB3aW4gPSAkKCB3aW5kb3cgKVxudmFyIGJvZCA9ICQoICdib2R5JyApXG52YXIgbXIgPSByZXF1aXJlKCAnLi9tcicgKVxudmFyIGNlbnRlciA9ICQoJyNtci1jZW50ZXInKVxudmFyIGxjb24gPSAkKCcjbG9nby1jb24nKVxudmFyIGxvZ28gPSAkKCcjbG9nby1jb24gaW1nJylcbnZhciB0b3BXcmFwID0gJCgnI3RvcC13cmFwJylcbnZhciB0b3AgPSAkKCcjdG9wJylcbnZhciBNSURfWCA9IGlzTW9iaWxlID8gMTggOiA0MlxudmFyIE1JRF9ZID0gaXNNb2JpbGUgPyAxNiA6IDMxXG52YXIgY2FwMSwgY2FwMiwgY2FwM1xudmFyIG1hc2sgPSBtci5jcmVhdGVNYXNrKClcblxuY2VudGVyLmFwcGVuZCggbXIuY29udGFpbmVyIClcblxubWFzay5zZXRJbWFnZSggJ2ltZy9vLWxvY2sucG5nJyxcblx0aXNNb2JpbGUgPyAxMyA6IDI3LFxuXHRpc01vYmlsZSA/IDEwIDogMTksXG5cdGlzTW9iaWxlID8gMTkgOiAzOSxcblx0aXNNb2JpbGUgPyAxMyA6IDI1XG4pXG5cbiQoIGluaXQgKVxuXG5mdW5jdGlvbiBpbml0KCkge1xuXHRtci5zdGFydCgpXG5cdHdpbi5vbiggJ3Jlc2l6ZScsIHJlc2l6ZSApXG5cdC8vd2luLm9uKCAnc2Nyb2xsJywgb25TY3JvbGwgKVxuXHRyZXNpemUoKVxuXHQkKCcjbWFpbk1lbnUnKS5jc3Moe2Rpc3BsYXk6ICdibG9jayd9KVxuXHRzZXRUaW1lb3V0KCBzaG93Q2FwdGlvbjEsIDEwMDAgKVxuXG59XG5cbmZ1bmN0aW9uIHNob3dCYW5uZXJzKCkge1xuXG5cdHZhciBiYW5uZXJzID0gWyAnVEhFIEZVVFVSRScsICdJUyBOT1cnLCAnQklUTE9YJyBdXG5cdG5leHRCYW5uZXIoKVxuXG5cdGZ1bmN0aW9uIG5leHRCYW5uZXIoKSB7XG5cdFx0dmFyIGJhbm5lciA9IGJhbm5lcnMuc2hpZnQoKVxuXHRcdGJhbm5lcnMucHVzaCggYmFubmVyIClcblx0XHRtci5zdGFydEJhbm5lciggYmFubmVyIClcblx0XHRzZXRUaW1lb3V0KCBzdG9wQmFubmVyLCAxMDAwMCApXG5cdFx0ZnVuY3Rpb24gc3RvcEJhbm5lcigpIHtcblx0XHRcdG1yLnN0b3BCYW5uZXIoKVxuXHRcdFx0c2V0VGltZW91dCggbmV4dEJhbm5lciwgMzAwMCApXG5cdFx0fVxuXHR9XG5cbn1cblxuZnVuY3Rpb24gc2hvd0NhcHRpb24xKCkge1xuXHRjYXAxID0gbXIuY3JlYXRlQ2FwdGlvbigpXG5cdGNhcDEuc3RhcnQoICdUIEggRSAgIEYgVSBUIFUgUiBFJywgTUlEX1ggLSA1LCBNSURfWSApXG5cdGNhcDEub25Mb2NrZWQgPSBmdW5jdGlvbigpIHtcblx0XHRzZXRUaW1lb3V0KCBzaG93Q2FwdGlvbjIsIDUwMCApXG5cdH1cbn1cblxuZnVuY3Rpb24gc2hvd0NhcHRpb24yKCkge1xuXHRjYXAyID0gbXIuY3JlYXRlQ2FwdGlvbigpXG5cdGNhcDIuc3RhcnQoICdJIFMgICBOIE8gVycsIE1JRF9YLCBNSURfWSApXG5cdGNhcDIub25Mb2NrZWQgPSBmdW5jdGlvbigpIHtcblx0XHRzZXRUaW1lb3V0KCBsYXVuY2hNYXNrLCA1MDAgKVxuXHR9XG59XG5cbmZ1bmN0aW9uIGxhdW5jaE1hc2soKSB7XG5cdG1hc2sub25GaW5pc2hlZCA9IGZ1bmN0aW9uKCkge1xuXHRcdGNvbnNvbGUubG9nKCAnbWFzayBmaW5pc2hlZCcgKVxuXHRcdGxvZ28uYWRkQ2xhc3MoJ2VudGVyJylcblx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7bG9nby5hZGRDbGFzcygnZmluaXNoJyk7IHNob3dCYW5uZXJzKCl9LCAyMDAwIClcblx0XHRtYXNrLnN0b3AoKVxuXHR9XG5cdG1hc2subGF1bmNoKClcbn1cblxuZnVuY3Rpb24gb25TY3JvbGwoKSB7XG5cdHZhciBzdCA9IHdpbi5zY3JvbGxUb3AoKVxuXHR2YXIgd2ggPSB3aW4uaGVpZ2h0KClcblx0dmFyIGNoID0gY2VudGVyLmhlaWdodCgpXG5cdHZhciB0Z3QgPSB3aCAtIDEwMFxuXHR2YXIgcGVyYyA9IHN0IC8gdGd0XG5cdHZhciBoaCA9IGNoICogMC41XG5cdGlmKCBwZXJjID49IDEgKSBwZXJjID0gMVxuXHR2YXIgYm90ID0gY2ggLyAyIC0gcGVyYyAqIGhoXG5cblx0dG9wV3JhcC5jc3Moe2hlaWdodDogd2ggLSB0Z3QgKiBwZXJjIH0pXG5cdGlmKCBwZXJjID09PSAxICkge1xuXHRcdGJvZC5hZGRDbGFzcyggJ2Z1bGxpbicgKVxuXHR9IGVsc2Uge1xuXHRcdGJvZC5yZW1vdmVDbGFzcyggJ2Z1bGxpbicgKVxuXHR9XG5cbn1cblxuZnVuY3Rpb24gcmVzaXplKCkge1xuXG5cdHZhciB3ID0gd2luLndpZHRoKClcblx0dmFyIGggPSB3aW4uaGVpZ2h0KClcblx0dmFyIGR3ID0gdyAvIG1yLndpZHRoXG5cdHZhciBkaCA9IGggLyBtci5oZWlnaHRcblx0dmFyIGYgPSBkdyA+IGRoID8gZHcgOiBkaFxuXHR2YXIgY3NzID0ge1xuXHRcdHdpZHRoOiBmbG9vciggbXIud2lkdGggKiBmKSxcblx0XHRoZWlnaHQ6IGZsb29yKCBtci5oZWlnaHQgKiBmIClcblx0fVxuXG5cdGlmKCBjc3Mud2lkdGggJSAyICkgY3NzLndpZHRoICs9IDFcblx0aWYoIGNzcy5oZWlnaHQgJSAyICkgY3NzLmhlaWdodCArPSAxXG5cblx0JCgnI21haW5Db250ZW50JykuY3NzKCB7bWFyZ2luVG9wOiBoIH0pXG5cblx0bXIuY29udGFpbmVyLmNzcyhjc3MpXG5cdG9uU2Nyb2xsKClcblxufVxuIl19
