const exec = require( 'child_process' ).exec

run( 'intro', 'nodemon -w src/intro -e js --exec browserify src/intro -o www/intro.js --debug' )
run( 'style', 'nodemon -w less -e less --exec lessc less/_main.less www/style.css' )

function run( id, cmd ) {
	const proc = exec( cmd )
	proc.stdout.on( 'data', (data) => {
		console.log( id + '-log:', data )
	})
	proc.stderr.on( 'data', (data) => {
		console.log( id + '-error:', data )
	})
}
