var isMobile = require( './isMobile' )

var mr = new MatrixRain({
	numCols: isMobile ? 45 : 91,
	numRows: isMobile ? 31 : 61,
	placement: 'center'
})

mr.container.css({
	position: 'relative',
	overflow: 'hidden',
	backgroundColor: 'black'
})

module.exports = mr
