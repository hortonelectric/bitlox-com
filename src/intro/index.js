var isMobile = require( './isMobile' )
var floor = Math.floor
var win = $( window )
var bod = $( 'body' )
var mr = require( './mr' )
var center = $('#mr-center')
var lcon = $('#logo-con')
var logo = $('#logo-con img')
var topWrap = $('#top-wrap')
var top = $('#top')
var MID_X = isMobile ? 18 : 42
var MID_Y = isMobile ? 16 : 31
var cap1, cap2, cap3
var mask = mr.createMask()

center.append( mr.container )

mask.setImage( 'img/o-lock.png',
	isMobile ? 13 : 27,
	isMobile ? 10 : 19,
	isMobile ? 19 : 39,
	isMobile ? 13 : 25
)

$( init )

function init() {
	mr.start()
	win.on( 'resize', resize )
	//win.on( 'scroll', onScroll )
	resize()
	$('#mainMenu').css({display: 'block'})
	setTimeout( showCaption1, 1000 )

}

function showBanners() {

	var banners = [ 'THE FUTURE', 'IS NOW', 'BITLOX' ]
	nextBanner()

	function nextBanner() {
		var banner = banners.shift()
		banners.push( banner )
		mr.startBanner( banner )
		setTimeout( stopBanner, 10000 )
		function stopBanner() {
			mr.stopBanner()
			setTimeout( nextBanner, 3000 )
		}
	}

}

function showCaption1() {
	cap1 = mr.createCaption()
	cap1.start( 'T H E   F U T U R E', MID_X - 5, MID_Y )
	cap1.onLocked = function() {
		setTimeout( showCaption2, 500 )
	}
}

function showCaption2() {
	cap2 = mr.createCaption()
	cap2.start( 'I S   N O W', MID_X, MID_Y )
	cap2.onLocked = function() {
		setTimeout( launchMask, 500 )
	}
}

function launchMask() {
	mask.onFinished = function() {
		console.log( 'mask finished' )
		logo.addClass('enter')
		setTimeout(function(){logo.addClass('finish'); showBanners()}, 2000 )
		mask.stop()
	}
	mask.launch()
}

function onScroll() {
	var st = win.scrollTop()
	var wh = win.height()
	var ch = center.height()
	var tgt = wh - 100
	var perc = st / tgt
	var hh = ch * 0.5
	if( perc >= 1 ) perc = 1
	var bot = ch / 2 - perc * hh

	topWrap.css({height: wh - tgt * perc })
	if( perc === 1 ) {
		bod.addClass( 'fullin' )
	} else {
		bod.removeClass( 'fullin' )
	}

}

function resize() {

	var w = win.width()
	var h = win.height()
	var dw = w / mr.width
	var dh = h / mr.height
	var f = dw > dh ? dw : dh
	var css = {
		width: floor( mr.width * f),
		height: floor( mr.height * f )
	}

	if( css.width % 2 ) css.width += 1
	if( css.height % 2 ) css.height += 1

	$('#mainContent').css( {marginTop: h })

	mr.container.css(css)
	onScroll()

}
